<?php

declare(strict_types=1);

namespace Shd\MgEventCategories\ContaoManager;

use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Shd\MgEventCategories\MgEventCategories;

class Plugin implements BundlePluginInterface {
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(MgEventCategories::class)
                ->setLoadAfter([
                    'Contao\CoreBundle\ContaoCoreBundle',
                    'Contao\CalendarBundle\ContaoCalendarBundle',
                ]),
        ];
    }
}