<?php
namespace Shd\MgEventCategories;


use Contao\Backend;
use Contao\Database;
use Contao\System;

class EventBackend
{
    /**
     * Import the back end user object
     */
//    public function __construct()
//    {
//        parent::__construct();
//        $this->import('BackendUser', 'User');
//    }

    public function setDefaultCategories($table, $id)
    {
        $catDefault = BackendUser::getInstance()->mgEventCatDefault;
        if($table == "tl_calendar_events" && is_array($catDefault) && count($catDefault) > 0) {
            Database::getInstance()->prepare("UPDATE tl_calendar_events SET categories=? WHERE id=?")->execute(serialize($catDefault), $id);
        }
    }
}

