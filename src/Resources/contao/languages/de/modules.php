<?php

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['mgEventCat'] = array('Eventkategorien', 'Veranstaltungskategorien verwalten');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['mg_event_filter']['0'] = "Event Kategoriefilter";
$GLOBALS['TL_LANG']['FMD']['mg_event_filter']['1'] = "Filtert eine Eventliste nach Kategorien [mg_event_categories].";
