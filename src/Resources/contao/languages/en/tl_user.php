<?php
/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_user']['mgEventCat']               = array('Manage event categories', 'enable the user to manage event categories');
$GLOBALS['TL_LANG']['tl_user']['default_event_categories']  = array('Default event categories', 'event categories which should be automatically assigned to new events, created by this user/group.');
$GLOBALS['TL_LANG']['tl_user']['mg_evt_cat_legend']        = "Event categories";
