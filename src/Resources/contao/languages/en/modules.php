<?php

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['mgEventCat'] = array('Event categories', 'manage event categories');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['mg_event_filter']['0'] = "Event Category filter";
$GLOBALS['TL_LANG']['FMD']['mg_event_filter']['1'] = "filters an event list by categories [mg_event_categories].";
