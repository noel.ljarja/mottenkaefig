<?php
/**
 * Add a palette to tl_user
 */
$GLOBALS['TL_DCA']['tl_user']['palettes']['extend'] = str_replace('{calendars_legend}', '{mg_evt_cat_legend},mgEventCat,mgEventCatDefault;{calendars_legend}', $GLOBALS['TL_DCA']['tl_user']['palettes']['extend']);
$GLOBALS['TL_DCA']['tl_user']['palettes']['custom'] = str_replace('{calendars_legend}', '{mg_evt_cat_legend},mgEventCat,mgEventCatDefault;{calendars_legend}', $GLOBALS['TL_DCA']['tl_user']['palettes']['custom']);

/**
 * Add a new field to tl_user
 */
$GLOBALS['TL_DCA']['tl_user']['fields']['mgEventCat'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_user']['mgEventCat'],
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'sql'                     => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_user']['fields']['mgEventCatDefault'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_user']['default_event_categories'],
    'exclude'                 => true,
    'inputType'               => 'checkboxWizard',
    'foreignKey'              => 'tl_mg_event_cat.title',
    'eval'                    => array('tl_class'=>'clr', 'multiple'=>true, 'fieldType'=>'checkbox', 'foreignTable'=>'tl_mg_event_cat', 'titleField'=>'title', 'searchField'=>'title'),
    'sql'                     => "blob NULL"
);
