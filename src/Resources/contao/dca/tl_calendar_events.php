<?php
use Shd\MgEventCategories\EventBackend;
$GLOBALS['TL_DCA']['tl_calendar']['config']['oncreate_callback'][] =[EventBackend::class,'setDefaultCategories'];

$GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['default'] = str_replace(";{date_legend}", ";{cat_legend:hide},categories;{date_legend}", $GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['default']);
$GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['internal'] = str_replace(";{date_legend}", ";{cat_legend:hide},categories;{date_legend}", $GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['internal']);
$GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['article'] = str_replace(";{date_legend}", ";{cat_legend:hide},categories;{date_legend}", $GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['article']);
$GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['external'] = str_replace(";{date_legend}", ";{cat_legend:hide},categories;{date_legend}", $GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['external']);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['categories'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_calendar_events']['categories'],
    'exclude'                 => true,
    'filter'                  => true,
    'inputType'               => 'checkboxWizard',
    'foreignKey'              => 'tl_mg_event_cat.title',
    'eval'                    => array('tl_class'=>'clr', 'multiple'=>true, 'fieldType'=>'checkbox', 'foreignTable'=>'tl_mg_event_cat', 'titleField'=>'title', 'searchField'=>'title'),
    'sql'                     => "blob NULL"
);

